﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using smart_coach.Models;

namespace smart_coach.Controllers
{
    public class AbonnementsController : ApiController
    {
        private SMARTCOACHEntities1 db = new SMARTCOACHEntities1();

        // GET: api/Abonnements
        public IQueryable<Abonnement> GetAbonnements()
        {
            return db.Abonnements;
        }

        // GET: api/Abonnements/5
        [ResponseType(typeof(Abonnement))]
        public IHttpActionResult GetAbonnement(int id)
        {
            Abonnement abonnement = db.Abonnements.Find(id);
            if (abonnement == null)
            {
                return NotFound();
            }

            return Ok(abonnement);
        }

        // PUT: api/Abonnements/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutAbonnement(int id, Abonnement abonnement)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != abonnement.idAbonn)
            {
                return BadRequest();
            }

            db.Entry(abonnement).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AbonnementExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Abonnements
        [ResponseType(typeof(Abonnement))]
        public IHttpActionResult PostAbonnement(Abonnement abonnement)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Abonnements.Add(abonnement);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = abonnement.idAbonn }, abonnement);
        }

        // DELETE: api/Abonnements/5
        [ResponseType(typeof(Abonnement))]
        public IHttpActionResult DeleteAbonnement(int id)
        {
            Abonnement abonnement = db.Abonnements.Find(id);
            if (abonnement == null)
            {
                return NotFound();
            }

            db.Abonnements.Remove(abonnement);
            db.SaveChanges();

            return Ok(abonnement);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool AbonnementExists(int id)
        {
            return db.Abonnements.Count(e => e.idAbonn == id) > 0;
        }
    }
}