﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using smart_coach.Models;

namespace smart_coach.Controllers
{
    public class CommentairesController : ApiController
    {
        private SMARTCOACHEntities1 db = new SMARTCOACHEntities1();

        // GET: api/Commentaires
        public IQueryable<Commentaire> GetCommentaires()
        {
            return db.Commentaires;
        }

        // GET: api/Commentaires/5
        [ResponseType(typeof(Commentaire))]
        public IHttpActionResult GetCommentaire(int id)
        {
            Commentaire commentaire = db.Commentaires.Find(id);
            if (commentaire == null)
            {
                return NotFound();
            }

            return Ok(commentaire);
        }

        // PUT: api/Commentaires/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutCommentaire(int id, Commentaire commentaire)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != commentaire.idCmnt)
            {
                return BadRequest();
            }

            db.Entry(commentaire).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CommentaireExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        //participant au direct ajout un commentaire
        // POST: api/Commentaires
        [ResponseType(typeof(Commentaire))]
        public IHttpActionResult PostCommentaire(Commentaire commentaire)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            Commentaire c = new Commentaire();
            c.contenu = commentaire.contenu;
            c.idDirect = commentaire.idDirect;

            db.Commentaires.Add(c);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = commentaire.idCmnt }, commentaire);
        }

        // DELETE: api/Commentaires/5
        [ResponseType(typeof(Commentaire))]
        public IHttpActionResult DeleteCommentaire(int id)
        {
            Commentaire commentaire = db.Commentaires.Find(id);
            if (commentaire == null)
            {
                return NotFound();
            }

            db.Commentaires.Remove(commentaire);
            db.SaveChanges();

            return Ok(commentaire);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool CommentaireExists(int id)
        {
            return db.Commentaires.Count(e => e.idCmnt == id) > 0;
        }
    }
}