using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using smart_coach.Models;

namespace smart_coach.Controllers
{
    public class DirectsController : ApiController
    {
        private SMARTCOACHEntities1 db = new SMARTCOACHEntities1();

        // GET: api/Directs
        public IQueryable<Direct> GetDirects()
        {
            return db.Directs;
        }

        // GET: api/Directs/5
        [ResponseType(typeof(Direct))]
        public IHttpActionResult GetDirect(int id)
        {
            Direct direct = db.Directs.Find(id);
            if (direct == null)
            {
                return NotFound();
            }

            return Ok(direct);
        }

        // PUT: api/Directs/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutDirect(int id, Direct direct)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != direct.idDirect)
            {
                return BadRequest();
            }

            db.Entry(direct).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Directs
        [ResponseType(typeof(Direct))]
        public IHttpActionResult PostDirect(Direct direct)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            Direct d = new Direct();
            d.libelle = direct.libelle;
            d.description = direct.description;
            d.duree = direct.duree;
            d.idCat = direct.idCat;
            //d.idCoach = id_user;
            d.idCoach = direct.idCoach;

            db.Directs.Add(d);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = direct.idDirect }, direct);
        }

        // POST: api/Directs
        // CoachPlanifierDirect
        [ResponseType(typeof(Direct))]
        [HttpPost]
        public IHttpActionResult PostPlanifierDirect(Direct direct, int id_user)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            Direct d= new Direct();
            d.libelle = direct.libelle;
            d.description = direct.description;
            d.duree = direct.duree;
            d.idCat = direct.idCat;
            d.idCoach = id_user;

            db.Directs.Add(d);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = direct.idDirect }, direct);
        }


        // DELETE: api/Directs/5
        [ResponseType(typeof(Direct))]
        public IHttpActionResult DeleteDirect(int id)
        {
            Direct direct = db.Directs.Find(id);
            if (direct == null)
            {
                return NotFound();
            }

            db.Directs.Remove(direct);
            db.SaveChanges();

            return Ok(direct);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool DirectExists(int id)
        {
            return db.Directs.Count(e => e.idDirect == id) > 0;
        }
    }
}
