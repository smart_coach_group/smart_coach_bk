﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using smart_coach.Models;

namespace smart_coach.Controllers
{
    public class TypeUtilisateursController : ApiController
    {
        private SMARTCOACHEntities1 db = new SMARTCOACHEntities1();

        // GET: api/TypeUtilisateurs
        public IQueryable<TypeUtilisateur> GetTypeUtilisateurs()
        {
            return db.TypeUtilisateurs;
        }

        // GET: api/TypeUtilisateurs/5
        [ResponseType(typeof(TypeUtilisateur))]
        public IHttpActionResult GetTypeUtilisateur(int id)
        {
            TypeUtilisateur typeUtilisateur = db.TypeUtilisateurs.Find(id);
            if (typeUtilisateur == null)
            {
                return NotFound();
            }

            return Ok(typeUtilisateur);
        }

        // PUT: api/TypeUtilisateurs/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutTypeUtilisateur(int id, TypeUtilisateur typeUtilisateur)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != typeUtilisateur.idType)
            {
                return BadRequest();
            }

            db.Entry(typeUtilisateur).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TypeUtilisateurExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/TypeUtilisateurs
        [ResponseType(typeof(TypeUtilisateur))]
        public IHttpActionResult PostTypeUtilisateur(TypeUtilisateur typeUtilisateur)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.TypeUtilisateurs.Add(typeUtilisateur);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = typeUtilisateur.idType }, typeUtilisateur);
        }

        // DELETE: api/TypeUtilisateurs/5
        [ResponseType(typeof(TypeUtilisateur))]
        public IHttpActionResult DeleteTypeUtilisateur(int id)
        {
            TypeUtilisateur typeUtilisateur = db.TypeUtilisateurs.Find(id);
            if (typeUtilisateur == null)
            {
                return NotFound();
            }

            db.TypeUtilisateurs.Remove(typeUtilisateur);
            db.SaveChanges();

            return Ok(typeUtilisateur);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool TypeUtilisateurExists(int id)
        {
            return db.TypeUtilisateurs.Count(e => e.idType == id) > 0;
        }
    }
}