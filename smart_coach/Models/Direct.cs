//------------------------------------------------------------------------------
// <auto-generated>
//     Ce code a été généré à partir d'un modèle.
//
//     Des modifications manuelles apportées à ce fichier peuvent conduire à un comportement inattendu de votre application.
//     Les modifications manuelles apportées à ce fichier sont remplacées si le code est régénéré.
// </auto-generated>
//------------------------------------------------------------------------------

namespace smart_coach.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Direct
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Direct()
        {
            this.Commentaires = new HashSet<Commentaire>();
        }
    
        public int idDirect { get; set; }
        public string libelle { get; set; }
        public string description { get; set; }
        public Nullable<System.TimeSpan> duree { get; set; }
        public Nullable<int> nbVues { get; set; }
        public Nullable<int> idCat { get; set; }
        public Nullable<int> idUtilisateur { get; set; }
        public Nullable<int> idCoach { get; set; }
    
        public virtual Categorie Categorie { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Commentaire> Commentaires { get; set; }
        public virtual Utilisateur Utilisateur { get; set; }
        public virtual Utilisateur Utilisateur1 { get; set; }
    }
}
